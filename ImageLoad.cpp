#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  Mat image = imread("sample.jpg");

  if (image.empty())
  {
    cout << "Could not open or find image" << endl;
    cin.get();
    return -1;
  }

  String windowName = "Snoopy";
  namedWindow(windowName, WINDOW_NORMAL);
  imshow(windowName, image);
  waitKey(0);
  destroyWindow(windowName);

  return 0;
}

