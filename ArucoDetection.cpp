#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
using namespace aruco;

int main(int argc, char const *argv[])
{
  cout << "This is for the aruco basic marker detection" << endl;
  
  Mat inputImage = imread("arucoImage.jpg");

  if (inputImage.empty())
  {
    cout << "Could not open or find the image" << endl;
    cin.get();
    return -1;
  }

  vector<int> markerIds;
  vector<vector<Point2f>> markerCorners, rejectedCandidates;
  Ptr<Dictionary> dictionary = getPredefinedDictionary(DICT_4X4_50);
  
  detectMarkers(
    inputImage, 
    dictionary, 
    markerCorners, 
    markerIds
  );

  if (markerIds.size() == 0)
  {
    cout << "unable to detect Aruco markers." << endl;
    return -1;
  }

  drawDetectedMarkers(inputImage, markerCorners, markerIds);

  if (!imwrite("markedMarkers.jpg", inputImage))
  {
    cout << "Failed to save the marked image" << endl;
    cin.get();
    return -1;
  }

  cout << "Marked image has been successfully saved to the file" << endl;
  
  return 0;
}
