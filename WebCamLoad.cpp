#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  VideoCapture cap(0);
  
  if (!cap.isOpened())
  {
    cout << "Cannot open the webcam..." << endl;
    cin.get();
    return -1;
  }

  double videoWidth = cap.get(CAP_PROP_FRAME_WIDTH);
  double videoHeight = cap.get(CAP_PROP_FRAME_HEIGHT);

  cout << "Resolution of the video, width: " << videoWidth << " height: " << videoHeight << endl;

  string windowName = "My Camera Feed";
  namedWindow(windowName);

  while(true)
  {
    Mat frame;
    bool isRead = cap.read(frame);

    if (!isRead)
    {
      cout << "Webcam is disconnected" << endl;
      cin.get();
      break;
    }

    imshow(windowName, frame);

    if (waitKey(10) == 27)
    {
      cout << "Escape key was pressed by user..." << endl;
      break;
    }
  }

  return 0;
}
