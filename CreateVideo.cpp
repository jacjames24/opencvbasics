#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  VideoCapture cap(0);

  if (!cap.isOpened())
  {
    cout << "Cannot open the video camera" << endl;
    cin.get();
    return -1;
  }

  int frameWidth = static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH));
  int frameHeight = static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT));

  Size frameSize(frameWidth, frameHeight);
  int framesPerSecond = 10;

  VideoWriter outVideoWriter("myVideo.avi", 
    VideoWriter::fourcc('M', 'J', 'P', 'G'), 
    framesPerSecond, frameSize, true);
  
  if (!outVideoWriter.isOpened())
  {
    cout << "Cannot save the video to a file." << endl;
    cin.get();
    return -1;
  }

  String windowName = "My Camera Feed";
  namedWindow(windowName);

  while(true)
  {
    Mat frame;
    bool isFrameRead = cap.read(frame);

    if (!isFrameRead)
    {
      cout << "Video camera is disconnected" << endl;
      cin.get();
      break;
    }

    outVideoWriter.write(frame);
    imshow(windowName, frame);

    if (waitKey(10) == 27)
    {
      cout << "Esc key was pressed by the user. Stopping the video." << endl;
      break;
    }
  }

  outVideoWriter.release();

  return 0;
}
