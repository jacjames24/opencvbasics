#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
using namespace aruco;

int main(int argc, char const *argv[])
{
  cout << "This is for the aruco basic marker creation" << endl;

  Mat outputImage(700, 700, CV_8UC1);
  outputImage = Scalar(255);

  Rect markerRect(100, 100, 500, 500);
  Mat outputMarker(outputImage, markerRect);

  Ptr<Dictionary> dictionary = getPredefinedDictionary(DICT_4X4_50);
  drawMarker(dictionary, 20, 500, outputMarker, 1);

  if (!imwrite("arucoImage.jpg", outputImage))
  {
    cout << "Failed to create the image" << endl;
    cin.get();
    return -1;
  }

  cout << "Aruco Image has been successfully saved to the file" << endl;

  return 0;
}
