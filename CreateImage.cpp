#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  Mat image(600, 800, CV_8UC3, Scalar(100, 250, 30));
  String windowName = "Window with Blank Image";
  namedWindow(windowName);
  imshow(windowName, image);
  waitKey(0);
  destroyWindow(windowName);
  
  return 0;
}
