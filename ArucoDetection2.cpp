#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/highgui.hpp>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace cv;
using namespace std;

// Function to write ArUco markers
void createArucoMarkers()
{

    // Create image to hold the marker plus surrounding white space
    Mat outputImage(700, 700, CV_8UC1);
    // Fill the image with white
    outputImage = Scalar(255);
    // Define an ROI to write the marker into
    Rect markerRect(100, 100, 500, 500);
    Mat outputMarker(outputImage, markerRect);

    // Choose a predefined Dictionary of markers
    Ptr< aruco::Dictionary> markerDictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_50);
    // Write each of the markers to a '.jpg' image file
    for (int i = 0; i < 10; i++)
    {
        //Draw the marker into the ROI
        aruco::drawMarker(markerDictionary, i, 500, outputMarker, 1);
        ostringstream convert;
        string imageName = "4x4Marker_";
        convert << imageName << i << ".jpg";
        // Note we are writing outputImage, not outputMarker
        imwrite(convert.str(), outputImage);

    }
}

// Main body of the routine
int main(int argv, char** argc)
{
    createArucoMarkers();

    // Read a specific image
    Mat frame = imread("arucoImage.jpg", CV_LOAD_IMAGE_COLOR);
    // Define variables to store the output of marker detection
    vector<int> markerIds;
    vector<vector<Point2f>> markerCorners, rejectedCandidates;
    // Define a Dictionary type variable for marker detection
    Ptr<aruco::Dictionary> markerDictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_50);

    // Detect markers
    aruco::detectMarkers(frame, markerDictionary, markerCorners, markerIds);

    // Display the image
    namedWindow("Webcam", CV_WINDOW_AUTOSIZE);
    // Draw detected markers on the displayed image
    aruco::drawDetectedMarkers(frame, markerCorners, markerIds);
    // Show the image with the detected marker
    imshow("Webcam", frame);

    // If a marker was identified, show its ID
    if (markerIds.size() > 0) {
        cout << "\nmarker ID is:\t" << markerIds[0] << endl;
    }
    waitKey(0);

}