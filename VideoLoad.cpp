#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  VideoCapture cap("sample.mp4");
  
  if (!cap.isOpened())
  {
    cout << "Cannot open the file..." << endl;
    cin.get();
    return -1;
  }

  double fps = cap.get(CAP_PROP_FPS);
  cout << "Frames per second : " << fps << endl;

  String windowName = "My First Video";
  namedWindow(windowName, WINDOW_NORMAL);

  while(true)
  {
    Mat frame;
    bool bSuccess = cap.read(frame);

    if (!bSuccess)
    {
      cout << "Found the end of the video." << endl;
      break;
    }

    imshow(windowName, frame);
    if (waitKey(10) == 27)
    {
      cout << "Esc key is pressed by user. Stoppig the video" << endl;
      break;
    }
  }

  return 0;
}
