#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
using namespace aruco;

String getModeName(int mode)
{
  switch(mode)
  {
    case 0: return "DICT_4X4_50";
    case 1: return "DICT_4X4_100";
    case 2: return "DICT_4X4_250";
    case 3: return "DICT_4X4_1000";
    case 4: return "DICT_5X5_50";
    case 5: return "DICT_5X5_100";
    case 6: return "DICT_5X5_250";
    case 7: return "DICT_5X5_1000";
    case 8: return "DICT_6X6_50";
    case 9: return "DICT_6X6_100";
    case 10: return "DICT_6X6_250";
    case 11: return "DICT_6X6_1000";
    case 12: return "DICT_7X7_50";
    case 13: return "DICT_7X7_100";
    case 14: return "DICT_7X7_250";
    case 15: return "DICT_7X7_1000";
    case 16: return "DICT_ARUCO_ORIGINAL";
    case 17: return "DICT_APRILTAG_16h5";
    case 18: return "DICT_APRILTAG_25h9";
    case 19: return "DICT_APRILTAG_36h10";
    case 20: return "DICT_APRILTAG_36h11";
  }

  return "";
}

int main(int argc, char const *argv[])
{
  cout << "This is for the detection via the web cam" << endl;
  
  int i = 0;
  cout << "Please enter the corresponding mode (0 to 20) and press 'return', with 0 == DICT_4X4_50=0 (default), and 20 == DICT_APRILTAG_36h11" << endl;
  cin >> i;

  String mode = getModeName(i);
  if (mode.empty())
  {
    cout << "Invalid mode supplied." << endl;
    return -1;
  }

  cout << "The mode you entered is: " << i << " == " << mode << endl; 

  VideoCapture cap(0);
  
  if (!cap.isOpened())
  {
    cout << "Cannot open the webcam..." << endl;
    cin.get();
    return -1;
  }

  Ptr<Dictionary> dictionary = getPredefinedDictionary(i);

  string windowName = "My Camera Feed";
  namedWindow(windowName);

  while(cap.grab())
  {
    Mat image, imageCopy;
    cap.retrieve(image);
    image.copyTo(imageCopy);

    vector<int> ids;
    vector<vector<Point2f>> corners;
    detectMarkers(image, dictionary, corners, ids);

    if (ids.size() > 0)
    {
      drawDetectedMarkers(imageCopy, corners, ids);
    }

    imshow(windowName, imageCopy);

    if (waitKey(1) == 27)
    {
      cout << "Escape key was pressed by user..." << endl;
      break;
    }
  }

  return 0;
}
