#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
  Mat image = imread("sample.jpg");

  if (image.empty())
  {
    cout << "Could not open or find the image" << endl;
    cin.get();
    return -1;
  }

  bool isImageModified = imwrite("modified.jpg", image);

  if (!isImageModified)
  {
    cout << "Failed to save the image" << endl;
    cin.get();
    return -1;
  }

  cout << "Image is successfully saved to the file" << endl;

  String windowName = "The saved image";
  namedWindow(windowName);
  imshow(windowName, image);

  waitKey(0);
  destroyWindow(windowName);

  return 0;
}
